#!/bin/bash

# get-git-source.sh - Retrieve upstream's sources from a GIT repository

set -ex

PACKAGE=tendermint-go-process
REPO='https://github.com/tendermint/go-process'
SEP='~'
BASE_REL=${BASE_REL:-$(dpkg-parsechangelog 2>/dev/null | sed -ne 's/Version: \([0-9.]\+\)~.*/\1/p')}
TIMESTAMP="$(date +%Y%m%d)"
OLDDIR=${PWD}
GOS_DIR=${OLDDIR}/get-orig-source
REPACK_EXT=

if [ -z ${BASE_REL} ]; then
    echo 'Please run this script from the sources root directory.'
    exit 1
fi

rm -rf ${GOS_DIR}
mkdir -p ${GOS_DIR} && cd ${GOS_DIR}
git clone "${REPO}" "${PACKAGE}"

cd ${PACKAGE}/
GIT_DESCRIBE=$(git describe --always)
VERSION="${BASE_REL}${SEP}${TIMESTAMP}${SEP}0git${GIT_DESCRIBE}"
cd .. && mv ${PACKAGE} ${PACKAGE}-${VERSION}
tar cf ${OLDDIR}/${PACKAGE}_${VERSION}.orig.tar --exclude-vcs ${PACKAGE}-${VERSION}
gzip -9fn ${OLDDIR}/${PACKAGE}_${VERSION}.orig.tar
rm -rf ${GOS_DIR}
